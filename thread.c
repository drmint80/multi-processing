#include <syslog.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define prname "threading"
#define comm "cd /home/menbe/public_html; sh command"

char **glob_args;


void *exec();
pthread_mutex_t mutexl;



void logging(char *argument){
    openlog(prname,LOG_PID,LOG_SYSLOG);
    syslog(LOG_INFO,"%s",argument);
    closelog();
}


void *exec(){
    //execute command
    char *comms;
    comms = (char*)malloc(1024);
    sprintf(comms,"%s%s.sh",comm,glob_args[1]);
    pthread_mutex_lock(&mutexl);
    system(comms);
    pthread_mutex_unlock(&mutexl);
    free(comms);
}

void daemonize(){
    char currdir[1024];
    pid_t pid,sid;
    int i;

    if(getppid()==1) return;
    pid=fork();
    if(pid<0) exit(1); //child process yaranmadi!
    if(pid>0) exit(0); //initial process exit veziyyetdedir.
    sid = setsid();//yeni session id yaradiram child process ucun cunki parent process terefinden inherited deyilem artiq.

    if(sid<0){
        logging("qaqam zor: Session ID yaradilmadi!");
        exit(EXIT_FAILURE);
    }

    /*parent process terefinden acilan butun descriptorlari bagla.*/
    for(i=getdtablesize(); i>=0; --i) close(i);
    i=open("/dev/null",O_RDWR); dup(i); dup(i);
    umask(027);
    getcwd(currdir,1024);
    if(chdir(currdir)<0){
        //change current directory
        logging("qaqam zor: Failure on directory change stage!");
        exit(EXIT_FAILURE);
    }
}


void main(int argc,char *argv[]){
    pthread_t threads[5];
    long n;
    glob_args = argv;
    daemonize();
    while(1) {
        sleep(10);
        for(n=0; n<5; n++) {
            pthread_create(&threads[n],NULL,exec,(void *)n);
	    pthread_detach(threads[n]);
        }
    }
}